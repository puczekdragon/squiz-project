//call to remote address to retrive data
async function getData() {
    const response = await fetch('https://dujour.squiz.cloud/developer-challenge/data')
    .then((response) => response.json())
    .then((data) => {
        // console.log(data);
        createTable(data);

        //initial sort and input clearing, as browsers like to leave selected input ruining starting sorting 
        sortTableBy( document.querySelector('.sorting-selection input:checked').value );
        document.querySelector('.filter-form .country-input').value = '';
        document.querySelector('.filter-form .industry-input').value = '';
    });
  
}


//construct table from given data
function createTable(data) {

    data.forEach(element => {
        //creating single row and its cells for one array element
        const tableRow = document.createElement('div');
        const idField = document.createElement('span');
        const nameField = document.createElement('span');
        const countryField = document.createElement('span');
        const industryField = document.createElement('span');
        const numberOfEmployeesField = document.createElement('span');

        tableRow.classList.add('single-row');
        tableRow.dataset.visibilitystate = element.stateClass ? element.stateClass : 'visible'; //this data field is used for hiding undiserable results when searching
        idField.classList.add('id-cell');
        nameField.classList.add('name-cell');
        countryField.classList.add('country-cell');
        industryField.classList.add('industry-cell');
        numberOfEmployeesField.classList.add('numberOfEmployees-cell');

        //asigning values to the cells
        idField.innerHTML = element.id;
        nameField.innerHTML = element.name;
        countryField.innerHTML = element.country;
        industryField.innerHTML = element.industry;
        numberOfEmployeesField.innerHTML = element.numberOfEmployees;


        //appending cells to the row, then row to the table itself
        tableRow.appendChild(idField);
        tableRow.appendChild(nameField);
        tableRow.appendChild(countryField);
        tableRow.appendChild(numberOfEmployeesField);
        tableRow.appendChild(industryField);
        document.querySelector('.table-inner-wrapper').appendChild(tableRow);
    });
}

//clear table, typically called before creating new table
function clearTable() {
    document.querySelectorAll('.table-inner-wrapper .single-row').forEach((element) => {
        element.remove();
    });
}

//generate data array from existing table, to not make external call everytime when data is needed
function getDataFromGeneratedTable() {
    const data = [];
    document.querySelectorAll('.table-inner-wrapper .single-row').forEach((element) => {

        //pushing object with same structure as API returned originally, but with added stateClass field, for search result visibility
        data.push({
            'id' : element.querySelector('.id-cell').innerHTML,
            'name' : element.querySelector('.name-cell').innerHTML,
            'country' : element.querySelector('.country-cell').innerHTML,
            'industry' : element.querySelector('.industry-cell').innerHTML,
            'numberOfEmployees' : element.querySelector('.numberOfEmployees-cell').innerHTML,
            'stateClass' : element.dataset.visibilitystate,
        });
    });

    return data;
}

// ifAsc decide how its sorted, input -1 to reverse order;
function sortTableName(ifAsc = 1, data) {
    //generating new data in case no data was provided
    if(data === undefined) data = getDataFromGeneratedTable();

    const newData = data.sort((a, b) => {
        return a.name.localeCompare(b.name, undefined) * ifAsc;
    });
    clearTable();
    createTable(newData);
}

// ifAsc decide how its sorted, input -1 to reverse order;
function sortTablenumberOfEmployees(ifAsc = 1, data) {
    //generating new data in case no data was provided
    if(data === undefined) data = getDataFromGeneratedTable();

    const newData = data.sort((a, b) => {

        return ifAsc*(+a.numberOfEmployees - +b.numberOfEmployees); //+ sign here is used for quick conversion to number

    });
    clearTable();
    createTable(newData);
}

// ifAsc decide how its sorted, input -1 to reverse order;
function sortTableID(ifAsc = 1, data) {
    //generating new data in case no data was provided
    if(data === undefined) data = getDataFromGeneratedTable();

    const newData = data.sort((a, b) => {
        return ifAsc*(+a.id - +b.id);
        
    });
    clearTable();
    createTable(newData);
}

//pick on which field sort the table 
function sortTableBy(sortType) {
    switch(sortType){
        case 'id-asc':
            sortTableID();
            break;
        case 'name-asc':
            sortTableName();
            break;
        case 'name-desc':
            sortTableName(-1);
            break;
        case 'number-of-employees-asc':
            sortTablenumberOfEmployees();
            break;
        case 'number-of-employees-desc':
            sortTablenumberOfEmployees(-1);
            break;
        default:
            console.log("hmm, that's strange"); //this should never been loged, if it is, something went horribly wrong
    }
}

//sorting events asigments
function asignSorting() {
    document.querySelectorAll('.sorting-selection input').forEach((element) => {

        //asiging specific functions with proper parameters for each sorting option
        element.addEventListener('change', (event) => {

            sortTableBy(event.target.value);

        })
    });
}

//search for country and industry
function filterSearch() {
    document.querySelector('.filter-form').addEventListener('submit', (event)=> {
        event.preventDefault();

        document.querySelectorAll('.table-inner-wrapper .single-row').forEach((element) => {
            const country = element.querySelector('.country-cell').innerHTML.toLowerCase();
            const searchedCountry = event.target.querySelector('.country-input').value.toLowerCase();
            const industry = element.querySelector('.industry-cell').innerHTML.toLowerCase();
            const searchedIndustry = event.target.querySelector('.industry-input').value.toLowerCase();

            //this data field is used to set visibility state of single row, by default it should be visible
            if( !country.includes(searchedCountry) ) element.dataset.visibilitystate = 'hidden';
            else if( !industry.includes(searchedIndustry) ) element.dataset.visibilitystate = 'hidden';
            else element.dataset.visibilitystate = 'visible';

        });

    });
}

//clears all hidden flags and input fields
function clearSearch() {
    document.querySelector('.filter-form .reset-button').addEventListener('click', (event)=> {
        document.querySelector('.filter-form .country-input').value = '';
        document.querySelector('.filter-form .industry-input').value = '';

        //hidden fields are removed by on submit event function, declared in filterSearch(), as its called by button press
    });
    
}

window.addEventListener('load', (event) => {
    
    //initial data aquisition
    getData();

    //
    asignSorting();

    //
    filterSearch();

    //
    clearSearch();
    
});